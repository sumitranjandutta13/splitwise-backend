require("dotenv").config();
const cors = require("cors");
const express = require("express");
const { json } = require("express");
const app = express();
const corsOption = { origin: "http://localhost:4200" };
app.use(cors(corsOption));
app.use(json());
app.use(express.urlencoded({ extended: true }));
const mongoose = require("mongoose");
const router = require("./routers/api");
mongoose.connect(
  `mongodb+srv://vivek-kumar:${process.env.DB_PASSWORD}@cluster0.y4kv9.mongodb.net/Splitwise?retryWrites=true&w=majority`,
  {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false,
    useCreateIndex: true,
  },
);
const User = require("./models/users");
const Group = require("./models/groups");
app.use("/api", router);
const PORT = process.env.PORT || 3000;
app.get("/", async (req, res) => {
  res.send("hello world");
  const newId = await User.find({}, { email: 1, _id: 0 });
  console.log(newId);
});

app.listen(PORT, (err) => {
  console.log(`Running on PORT : ${PORT}`);
});
