const makeUnpaid = (total, payerArr) => {
  splitObj = {};
  splitObj["splitObj"] = {};
  payerArr.forEach((obj) => {
    let a = Object.entries(obj)[0];
    splitObj["splitObj"][a[0]] = a[1];
    //console.log(a);
  });
  splitObj = splitObj["splitObj"];
  let members = Object.keys(splitObj).length;
  avg = total / members;
  unpaid = [];
  for (let i of payerArr) {
    let a = Object.keys(i)[0];
    if (i[a] > avg) {
      x = a;
      y = i[a] - avg;
      obj = {};
      obj[x] = y;
      unpaid.push(obj);
    } else if (i[a] < avg) {
      x = a;
      y = i[a] - avg;
      obj = {};
      obj[x] = y;
      unpaid.push(obj);
    }
  }
  return unpaid;
};
// [ { '1': 80 }, { '2': -80 }, { '3': -20 }, { '4': 20 } ] /n [ [ '1', 80 ], [ '4', 20 ] ] /n [ [ '2', -80 ], [ '3', -20 ] ]
// { '1': {}, '4': {} }
const makeSettlements = (totalAmt, payerArr) => {
  splitObj = {};
  splitObj["splitObj"] = {};
  payerArr.forEach((obj) => {
    let a = Object.entries(obj)[0];
    splitObj["splitObj"][a[0]] = a[1];
    //console.log(a);
  });
  splitObj = splitObj["splitObj"];

  //Method: Split Equally
  const totalMember = Object.keys(splitObj).length;
  const amtPerPerson = totalAmt / totalMember;

  const split = {}; //this would contain the result

  const sharesObj = Object.keys(splitObj).reduce((sharesObj, member) => {
    sharesObj[member] = +(splitObj[member] - amtPerPerson).toFixed(2);
    return sharesObj;
  }, {});

  // Calculating Split
  const owedArr = [];
  const owesArr = [];
  Object.keys(sharesObj).forEach((member) => {
    if (sharesObj[member] >= 0) {
      owedArr.push({ member, share: sharesObj[member] });
    } else {
      owesArr.push({ member, share: sharesObj[member] });
    }
  });

  if (owedArr.length === 1) {
    // split[owedArr[0].member] = owesArr.map(memObj => {
    //   memObj.share = memObj.share*-1
    //   return memObj
    // })
    split[owedArr[0].member] = owesArr.reduce((owesAmt, memObj) => {
      owesAmt[memObj.member] = memObj.share * -1;
      return owesAmt;
    }, {});
  } else {
    // Sorting OwesArr in Asec order to get person with MAX OWING in the starting
    owesArr.sort((a, b) => {
      return a.share - b.share;
    });

    // Sorting OwedArr in Desc order to get person with MAX OWED in the starting
    owedArr.sort((a, b) => {
      return b.share - a.share;
    });

    let i = 0,
      j = 0;

    let remainingAmt = 0;
    let remainingOwe = 0;

    while (i < owedArr.length) {
      const { member: owedMem, share: owedShare } = owedArr[i];

      // split[owedMem] = []
      split[owedMem] = {};

      remainingAmt = owedShare;

      let temp;

      while (remainingAmt > 0 && j < owesArr.length) {
        const { member: owesMem, share: owesShare } = owesArr[j];
        remainingOwe = remainingOwe < 0 ? remainingOwe : owesShare;
        temp = remainingAmt;

        // console.log({owedMem,owedShare,remainingAmt,owesMem, owesShare, remainingAmt})

        remainingAmt = remainingAmt + remainingOwe;
        if (remainingAmt > 0) {
          split[owedMem][owesMem] = +remainingOwe * -1;
          remainingOwe = 0;
          j++;
        } else if (remainingAmt === 0) {
          split[owedMem][owesMem] = +remainingOwe * -1;
          i++;
          j++;
        } else {
          remainingOwe = owesShare + temp;
          split[owedMem][owesMem] = temp;
          i++;
        }

        // console.log({owedMem,owedShare,remainingAmt, temp,owesMem, owesShare, remainingOwe})

        // console.log("=====================================")
      }
    }
  }
  return split;
};
module.exports = { makeUnpaid, makeSettlements };
